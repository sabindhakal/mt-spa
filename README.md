## Create new microservice

```
create-single-spa --framework vue
```

### start micro services

```
./start-service.sh
```

### start root config

```
./start-root-config.sh
```
