import { registerApplication, start } from "single-spa";

registerApplication({
  name: "@inspstudio/header",
  app: () => System.import("@inspstudio/header"),
  activeWhen: "/",
  customProps: {
    website: "https://inspstudio.com",
  },
});

registerApplication({
  name: "@inspstudio/about",
  app: () => System.import("@inspstudio/about"),
  activeWhen: "/about",
});

start();
